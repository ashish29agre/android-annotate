package in.ashish29agre.annotate;

import android.app.Activity;
import android.widget.EditText;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.TextChange;
import com.googlecode.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_annotate)
public class AnnotateActivity extends Activity {

    @ViewById
    EditText editContent;
    @ViewById
    TextView resultContent;
    
    @TextChange
    void editContent() {
    	resultContent.setText(editContent.getText().toString());
    }


    
    
}
